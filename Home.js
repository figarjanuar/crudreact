//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import axios from 'axios'
import Swipeout from 'react-native-swipeout';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import AwesomeAlert from 'react-native-awesome-alerts';

// create a component
class Home extends Component {
    constructor(){
        super()
        this.state = {
          data: null,
          showAlert: false
        }
      }
    
      showAlert = () => {
        this.setState({
          showAlert: true
        });
      };
     
      hideAlert = () => {
        this.setState({
          showAlert: false
        });
      }
    
      componentDidMount = () => {
        axios.get('https://facebook.github.io/react-native/movies.json')
        .then((res) =>{
          // console.log(res.data.movies)
          this.setState({
            data: res.data.movies
          })
        })
        .catch(err => console.log(err)
        )
      }
    
      render() {
        const {showAlert} = this.state;
    
        let swipeoutBtns = [
          {
            text: 'Delete',
            type: 'delete',
            onPress:() => {
              this.showAlert()
            },
          }
        ]
    
        const moviesData = this.state.data
    
        return (
          <View style={styles.container}>
          {
            moviesData && 
            moviesData.map((data, i) => {
              return (
                <Swipeout autoClose={true} key={i} right={swipeoutBtns} style={styles.slide}>
                  <View style={{padding:13, backgroundColor:'#fff'}}>
                    <Text>{data.title}</Text>
                  </View>
                </Swipeout>
              )
            })
          }
          {/* alert */}
          <AwesomeAlert
              show={showAlert}
              showProgress={false}
              title="Warning"
              message="Are you sure delete this data?"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              showCancelButton={true}
              showConfirmButton={true}
              cancelText="No, cancel"
              confirmText="Yes, delete it"
              confirmButtonColor="#DD6B55"
              onCancelPressed={() => {
                this.hideAlert();
              }}
              onConfirmPressed={() => {
                this.hideAlert();
              }}
            />
          </View>
        )
    
      }
}

// define your styles
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    slide: {
      backgroundColor:'#fff',
      borderBottomWidth:.5,
      borderBottomColor:'#333333'
    }
  });

//make this component available to the app
export default Home;
